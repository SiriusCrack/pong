using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameOver : MonoBehaviour
{
    public GameObject gameOverUI;

    // Update is called once per frame
    void Update()
    {
        if(Ball.gameState == false){
            gameOverUI.SetActive(true);
            
            Time.timeScale = 0;
        }
    }
}

