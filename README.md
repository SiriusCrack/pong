# Getting started

1. Download the respective package for your system from the [Releases](https://gitlab.com/SiriusCrack/pong/-/releases) page.

## Linux
2. untar the package
3. run "pong.x86_64"

## Windows
2. unzip the package
3. run "pong.exe"

# Build
1. [Get Unity](https://unity.com/download)
2. clone this repository
3. open this repository as a unity project
4. in the menu bar go to **File > Build and Run** - *see [Unity Build Settings Documentation](https://docs.unity3d.com/Manual/BuildSettings.html) for more info*

## Build (for my specific set-up as per assignment)
1. Get Unity Hub
    ```sh
    yay -Syu unityhub-beta
    pacman -Syu cpio
    ```
2. Set up Unity Hub
    1. Sign in to Unity or create an account and acquire a Unity license
    2. Under the Installs tab to the left of the Unity Hub window, acquire install 2020.3.26f1
    3. Add any modules relevant to the system you wish to build for by clicking the gear on the top right of your recently acquired install and selecting "Add modules"
3. clone this repository
    ```sh
    git clone https://gitlab.com/SiriusCrack/pong.git
    ```
4. Under the Projects tab in Unity Hub, open the cloned pong directory
5. In the menu bar of the opened Unity window, go to **File > Build Settings...**
6. Set the target platform and select "Build And Run"